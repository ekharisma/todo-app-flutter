import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage();
  @override
  Widget build(BuildContext context) {
    final alucard = Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
          radius: 72.0,
          backgroundColor: Colors.transparent,
          backgroundImage: AssetImage('images/profile.jpg'),
        ),
      ),
    );

    final name = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Ekky Kharismadhany',
        style: TextStyle(fontSize: 28.0, color: Colors.white),
      ),
    );

    final institusi = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Institusi: Politeknik Elektronika Negeri Surabaya',
        style: TextStyle(fontSize: 16.0, color: Colors.white),
      ),
    );

    final asal = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Asal: Bojonegoro',
        style: TextStyle(fontSize: 16.0, color: Colors.white),
      ),
    );

    final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(28.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.blue,
          Colors.lightBlueAccent,
        ]),
      ),
      child: Column(
        children: <Widget>[alucard, name, institusi, asal],
      ),
    );
    return Scaffold(
      body: body,
    );
  }
}
