import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TodoPage extends StatefulWidget {
  const TodoPage();

  @override
  _TodoPageState createState() => _TodoPageState();
}

class _TodoPageState extends State<TodoPage> {
  TextEditingController taskController = TextEditingController();
  var task = [];
  @override
  Widget build(BuildContext context) {
    Widget tasks(String task) {
      return Card(
        elevation: 5.0,
        margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
        child: Container(
          padding: EdgeInsets.all(6.0),
          child: ListTile(
            title: Text('$task'),
          ),
        ),
      );
    }

    Future<void> _showDialog() {
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Add Task'),
              content: SingleChildScrollView(
                child: ListBody(
                  children: [
                    TextField(
                      controller: taskController,
                      keyboardType: TextInputType.text,
                    ),
                  ],
                ),
              ),
              actions: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextButton(
                    onPressed: () {
                      task.add(taskController.text);
                      Navigator.pop(context);
                    },
                    child: Text('Add Task'),
                  ),
                ),
              ],
            );
          });
    }

    final floatingBtn = FloatingActionButton(
      onPressed: () => _showDialog(),
      child: Icon(Icons.add),
    );

    final body = Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Colors.blue, Colors.blueAccent],
        ),
      ),
      child: SingleChildScrollView(
        child: Column(children: <Widget>[
          tasks('Membuat To Do List yang bisa ditambahin'),
          tasks('Membuat halaman login yang pas')
        ]),
      ),
    );

    return Scaffold(
      body: body,
      floatingActionButton: floatingBtn,
    );
  }
}
